#!/usr/bin/php
<?php
    require_once 'Connection.php';
    require_once 'Game.php';
    require_once 'Car.php';

    if( count( $argv ) < 5 )
    {
        die( "Usage: bot host port botname botkey\n" );
    }
    try
    {
        $connection = new Connection( $argv[ 1 ], $argv[ 2 ], $argv[ 3 ], $argv[ 4 ] );
    }
    catch( Exception $exception )
    {
        die( $exception->getMessage() . "\n" );
    }

    $game = new Game();

    try
    {
        while( !is_null( $msg = $connection->read_msg() ) )
        {
            if( $msg[ 'msgType' ] == 'carPositions' )
            {
                foreach( $msg[ 'data' ] as $carData )
                {
                    $id = $carData[ 'id' ][ 'name' ] . '|' . $carData[ 'id' ][ 'color' ];
                    /** @var Car $car */
                    $car = $game->getCars()[ $id ];
                    $car->setAngle( $carData[ 'angle' ] );
                    $car->setTrackPiece( $carData[ 'piecePosition' ][ 'pieceIndex' ] );
                    $car->setTrackPiecePosition( $carData[ 'piecePosition' ][ 'inPieceDistance' ] );
                    $car->setLap( $carData[ 'piecePosition' ][ 'lap' ] );
                    $car->setLane( $carData[ 'piecePosition' ][ 'lane' ][ 'startLaneIndex' ] );
                    $car->calculate();
                }
                $connection->write_msg( 'throttle', $game->getOurCar()->getThrottle() );
//                echo 'PreviousAngle: ' . round( $game->getOurCar()->previousAngle, 5 ) . "\n";
//                echo 'Angle: ' . round( $game->getOurCar()->getAngle(), 5 ) . "\n";
//                echo 'Throttle: ' . round( $game->getOurCar()->getThrottle(), 5 ) . "\n";
                continue;
            }
            if( $msg[ 'msgType' ] == 'yourCar' )
            {
                $game->setOurCarId( $msg[ 'data' ][ 'name' ] . '|' . $msg[ 'data' ][ 'color' ] );
                $connection->write_msg( 'ping', null );
                continue;
            }
            if( $msg[ 'msgType' ] == 'gameInit' )
            {
                $cars = array();
                foreach( $msg[ 'data' ][ 'race' ][ 'cars' ] as $car )
                {
                    $cars[ $car[ 'id' ][ 'name' ] . '|' . $car[ 'id' ][ 'color' ] ] = new Car( $car[ 'id' ][ 'name' ] . '|' . $car[ 'id' ][ 'color' ], $game );
                }
                $game->setCars( $cars );
                $game->setId( $msg[ 'gameId' ] );
                $game->setLaps( $msg[ 'data' ][ 'race' ][ 'raceSession' ][ 'laps' ] );
                $game->setTrack( $msg[ 'data' ][ 'race' ][ 'track' ] );
                $connection->write_msg( 'ping', null );
                continue;
            }

            if( $msg[ 'msgType' ] == 'crash' )
            {
                var_dump( $msg );
                echo 'Angle: ' . $game->getOurCar()->getAngle() . "\n";
                echo 'Throttle: ' . round( $game->getOurCar()->getThrottle(), 5 ) . "\n";
            }

            $connection->write_msg( 'ping', null );

    //        switch( $msg[ 'msgType' ] )
    //        {
    //            case 'join':
    //            case 'gameStart':
    //            case 'crash':
    //            case 'spawn':
    //            case 'lapFinished':
    //            case 'dnf':
    //            case 'finish':
    //            default:
    //                $connection->write_msg( 'ping', null );
    //        }
        }
    }
    catch( Exception $exception )
    {
        echo "\n" . $exception->getMessage() . "\n";
    }
