<?php

    class Car
    {
        /**
         * @var Game
         */
        private $game;

        /**
         * @var string
         */
        private $id;

        /**
         * @var float
         */
        private $throttle = 1;

        /**
         * @var float
         */
        private $angle;

        /**
         * @var float
         */
        public $previousAngle = 0;

        /**
         * @var integer
         */
        private $trackPiece;

        /**
         * @var float
         */
        private $trackPiecePosition;

        /**
         * @var integer
         */
        private $lap;

        /**
         * @var integer
         */
        private $lane;

        public function calculate()
        {
            if( $this->isNearAngle() && !$this->isOnAngle() )
            {
                $this->setThrottle( 0 );
                return;
            }
            if( $this->isNearAngle() && $this->getThrottle() > 0.5 )
            {
            }

            if( $this->angle >= 0 && $this->previousAngle >= 0 )
            {
                $angleChange = $this->angle - $this->previousAngle;
            }
            else if( $this->angle <= 0 && $this->previousAngle >= 0 )
            {
                $angleChange = $this->previousAngle - $this->angle;
            }
            elseif( $this->angle >= 0 && $this->previousAngle <= 0 )
            {
                $angleChange = $this->angle - $this->previousAngle;
            }
            else
            {
                $angleChange = abs( $this->angle - $this->previousAngle );
            }
            if( $angleChange > 0 )
            {
                if( abs( $this->angle ) < 3 && $this->getThrottle() < 1 )
                {
                    $this->setThrottle( 1 );
                }
                if( abs( $this->angle ) > 25 )
                {
                    $this->setThrottle( 0 );
                    return;
                }
//                echo "\n" . 'BIGGERANGLE: ' . $angleChange . "\n";
                if( $angleChange < 0.2 )
                {
                    if( abs( $this->angle ) < 10 )
                    {
                        $this->setThrottle( $this->getThrottle() - 0.1 );
                    }
                    return;
                }
                elseif( $angleChange < 1 )
                {
                    $this->setThrottle( $this->getThrottle() - 0.1 );
                }
                elseif( $angleChange < 1.5 )
                {
                    $this->setThrottle( $this->getThrottle() - 0.3 );
                }
                else
                {
                    $this->setThrottle( 0 );
                }

                return;
            }
            else if( $angleChange < 0 )
            {
//                echo "\n" . 'LowerAngle: ' . $angleChange . "\n";
//                $angleChange = abs( $angleChange );
                $this->setThrottle( $this->getThrottle() + 0.5 );
                return;
                if( $angleChange < 0.01 )
                {
                    $this->setThrottle( $this->getThrottle() + 0.1 );
                }
                else
                {
                    $this->setThrottle( $this->getThrottle() + 0.3 );
                }

                return;
            }
            else
            {
                $this->setThrottle( $this->getThrottle() + 0.6 );
                return;
            }

            if( !$this->isNearAngle() && $this->throttle != 1 )
            {
                $this->setThrottle( 1 );
            }
        }

        /**
         * @return bool
         */
        public function isOnAngle()
        {
            return array_key_exists( 'angle', $this->game->getTrack()[ 'pieces' ][ $this->getTrackPiece() ] );
        }
        /**
         * @return bool
         */
        private function isNearAngle()
        {
            $nextPiece = $this->game->getTrack()[ 'pieces' ][ ( $this->getTrackPiece() + 1 ) % $this->game->getTrackPiecesAmount() ];
            if( !array_key_exists( 'angle', $nextPiece ) )
            {
                return false;
            }

            $currentPiece = $this->game->getTrack()[ 'pieces' ][ $this->getTrackPiece() ];
            if( !array_key_exists( 'length', $currentPiece ) )
            {
                return false;
            }

            return $this->getTrackPiecePosition() + 45 > $currentPiece[ 'length' ];
        }

        public function __construct( $id, Game $game )
        {
            $this->game = $game;
            $this->id = $id;

            return $this;
        }

        public function getId()
        {
            return $this->id;
        }

        /**
         * @param float $throttle
         */
        public function setThrottle( $throttle )
        {
            if( $throttle > 1.0 )
            {
                $throttle = 1;
            }
            elseif( $throttle < 0.0 )
            {
                $throttle = 0;
            }
            $this->throttle = $throttle;
        }

        /**
         * @return float
         */
        public function getThrottle()
        {
            return $this->throttle;
        }

        /**
         * @param float $angle
         */
        public function setAngle( $angle )
        {
            $this->previousAngle = $this->angle;
            $this->angle = $angle;
        }

        /**
         * @return float
         */
        public function getAngle()
        {
            return $this->angle;
        }

        /**
         * @param int $lane
         */
        public function setLane( $lane )
        {
            $this->lane = $lane;
        }

        /**
         * @return int
         */
        public function getLane()
        {
            return $this->lane;
        }

        /**
         * @param int $lap
         */
        public function setLap( $lap )
        {
            $this->lap = $lap;
        }

        /**
         * @return int
         */
        public function getLap()
        {
            return $this->lap;
        }

        /**
         * @param int $trackPiece
         */
        public function setTrackPiece( $trackPiece )
        {
            $this->trackPiece = $trackPiece;
        }

        /**
         * @return int
         */
        public function getTrackPiece()
        {
            return $this->trackPiece;
        }

        /**
         * @param float $trackPiecePosition
         */
        public function setTrackPiecePosition( $trackPiecePosition )
        {
            $this->trackPiecePosition = $trackPiecePosition;
        }

        /**
         * @return float
         */
        public function getTrackPiecePosition()
        {
            return $this->trackPiecePosition;
        }
    }