<?php
    class Game
    {
        private $id;

        private $cars;

        private $laps;

        private $track;

        private $ourCarId;

        private $ourCar = null;

        private $trackPiecesAmount = 0;

        /**
         * @return int
         */
        public function getTrackPiecesAmount()
        {
            if( $this->trackPiecesAmount == 0 )
            {
                $this->trackPiecesAmount = count( $this->track[ 'pieces' ] );
            }

            return $this->trackPiecesAmount;
        }
        /**
         * @param mixed $cars
         */
        public function setCars( $cars )
        {
            $this->cars = $cars;
        }

        /**
         * @return mixed
         */
        public function getCars()
        {
            return $this->cars;
        }

        /**
         * @param mixed $id
         */
        public function setId( $id )
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param mixed $laps
         */
        public function setLaps( $laps )
        {
            $this->laps = $laps;
        }

        /**
         * @return mixed
         */
        public function getLaps()
        {
            return $this->laps;
        }

        /**
         * @param mixed $track
         */
        public function setTrack( $track )
        {
            $this->track = $track;
        }

        /**
         * @return mixed
         */
        public function getTrack()
        {
            return $this->track;
        }

        /**
         * @param mixed $ourCarId
         */
        public function setOurCarId( $ourCarId )
        {
            $this->ourCarId = $ourCarId;
        }

        /**
         * @return mixed
         */
        public function getOurCarId()
        {
            return $this->ourCarId;
        }

        /**
         * @return Car
         */
        public function getOurCar()
        {
            if( $this->ourCar == null )
            {
                $this->ourCar = $this->getCars()[ $this->getOurCarId() ];
            }

            return $this->ourCar;
        }
    }