<?php
    define( 'MAX_LINE_LENGTH', 1024 * 1024 );

    class Connection
    {
        protected $sock;

        function __construct( $host, $port, $botname, $botkey )
        {
            $this->connect( $host, $port, $botkey );
            $this->write_msg( 'join', array(
                'name' => $botname,
                'key' => $botkey
            ) );
        }

        function __destruct()
        {
            if( isset( $this->sock ) )
            {
                socket_close( $this->sock );
            }
        }

        protected function connect( $host, $port, $botkey )
        {
            $this->sock = @ socket_create( AF_INET, SOCK_STREAM, SOL_TCP );
            if( $this->sock === false )
            {
                throw new Exception( 'socket: ' . socket_strerror( socket_last_error() ) );
            }
            if( @ !socket_connect( $this->sock, $host, $port ) )
            {
                throw new Exception( $host . ': ' . $this->sockerror() );
            }
        }

        public function read_msg()
        {
            $line = @ socket_read( $this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ );
            if( $line === false )
            {
                throw new Exception( $this->sockerror() );
            }

            return json_decode( $line, true );
        }

        public function write_msg( $msgtype, $data )
        {
            $str = json_encode( array( 'msgType' => $msgtype, 'data' => $data ) ) . "\n";

            if( @socket_write( $this->sock, $str ) === false )
            {
                throw new Exception( 'write: ' . $this->sockerror() );
            }
        }

        protected function sockerror()
        {
            return socket_strerror( socket_last_error( $this->sock ) );
        }


    }
