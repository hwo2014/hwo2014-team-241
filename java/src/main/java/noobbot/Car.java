/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

/**
 *
 * @author Armageddon
 */
class CarContainer
{
    public String msgType;
    public Car[] data;
}

public class Car {
    
    public PlayerID id;
    public double angle;
    public Piece piecePosition;
    
    double Velocity;
    double Acceleration;
    
    double TickTime = 1.0 / 60.0;
    
    public void update(Car PrevCar)
    {
        if(PrevCar != null)
        {
            //Bardzo nami zarzucilo
            if(Math.abs(PrevCar.angle - angle) > 10.0f)
                Acceleration = 0.05f;
            //Czy sie powoli nie zblizamy do granicy
            else if(Math.abs(angle) > 20.0f)
                Acceleration = 0.1f;
            else
                Acceleration = 0.70f;

            //Costam
            Velocity += Acceleration * TickTime;
        }
        else
        {
            Acceleration = 1.0f;
        }
        
    }
}

class PlayerID {
    String name;
    String color;
}

class Piece{
    public float pieceIndex;
    public double inPieceDistance;
    public Lane lane;
    public float lap;
}

class Lane
{
    public float startLaneIndex, endLaneIndex;
}