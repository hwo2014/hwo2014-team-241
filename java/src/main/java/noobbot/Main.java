package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;

public class Main {

    public static void main(String... args) throws IOException {
        /*String host = args[0];
         int port = Integer.parseInt(args[1]);
         String botName = args[2];
         String botKey = args[3];*/

        String host = "testserver.helloworldopen.com";
        int port = 8091;
        String botName = "Kat";
        String botKey = "elyuYQW0Z+oETA";

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
        Game CurrentGame = new Game(1);
        
        while ((line = reader.readLine()) != null) {
            Car CurrCar = null;
            if (line.contains("carPosition")) {
                CurrCar = gson.fromJson(line, CarContainer.class).data[0];
                CurrentGame.tick(CurrCar);
                System.out.println("Angle: " + CurrCar.angle + " pieceIndex: "
                        + CurrCar.piecePosition.pieceIndex + " inPieceDistance: "
                        + CurrCar.piecePosition.inPieceDistance);
                
                System.out.println("Velocity: " + CurrCar.Velocity);

                send(new Throttle(CurrCar.Acceleration));
            } else {
                final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
                if (msgFromServer.msgType.equals("join")) {
                    System.out.println("Joined");
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    System.out.println("Race init");
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    System.out.println("Race end");
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    System.out.println("Race start");
                } else if (msgFromServer.msgType.equals("crash")) {
                    System.out.println("Wyjebales sie");
                } else {
                    send(new Ping());
                }
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
