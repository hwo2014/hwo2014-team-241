/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package noobbot;

import java.util.ArrayList;

/**
 *
 * @author Armageddon
 */
public class Game {
    
    Car[] Cars;
    Car[] OldCars;
    
    public Game(int NumCars)
    {
        Cars = new Car[NumCars];
        OldCars = new Car[NumCars];
    }
    
    public void tick(Car CurrentCar)
    {
        if(OldCars[0] == null)
            CurrentCar.Velocity = 0.0f;
        else
            CurrentCar.Velocity = OldCars[0].Velocity;
        
        Cars[0] = CurrentCar;
        
        
        CurrentCar.update(OldCars[0]);
        
        OldCars[0] = Cars[0];
    }
}
